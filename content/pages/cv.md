---
title: "Pietro Bonfa' - CV"
date: 2021-01-23T00:00:00Z
draft: false
type: "page"
---


## Biographical Sketch

I am an associate researcher (RTD-a) at the Department of Mathematical, Physical and Computer Sciences of the University of Parma.

In my research activity I investigate magnetic and superconducting states of condensed matter with local spectroscopic techniques, like Nuclear Magnetic Resonance and Muon Spin Rotation Relaxation Resonance. I also assist the experimental analysis with computational models of the microscopic interactions governing the observed data, mainly through Density Functional Theory based simulations.

I have extensive experience in performing experiments at large scale muon facilities and, at the same time, a solid knowledge of high performance and high throughput computing.

I've been involved in numerous European and Italian research projects and received the Italian academic qualification for the position of associate professor, class 02/B1.

## Personal Information

* Date and Place of Birth: 25 April 1986, Ponte dell'Olio, Italy 
* Nationality: Italian 
* ORCID Profile:  0000-0001-6358-3037 
* Scopus Profile: 54580558400
* E-mail: pietro.bonfa@unipv.it

## Education

* 01/01/2012 - 18/03/2015: Ph.D. in Physics, University of Parma, Parma (Italy) 

  * Thesis: "Ab initio modeling of positive muon implantation sites in crystalline solids".

* 01/10/2008 - 29/04/2011: Master's degree in Physics, University of Pavia, Pavia (Italy)

  * Grade: 110/110 cum laude
  * Major: Solid State Physics
  * Thesis: "Effects of isoelectronic substitutions on the microscopic properties of the parent compounds of La1111" 

* 01/08/2005 - 25/09/2008: Bachelor's degree in Physical Engineering, Politecnico di Milano, Milano (Italy) 

## Publications and Citations 

* Publications in Peer-Reviewed Journals: 31
* Conference Proceedings in Peer-Reviewed Journals: 2

### Metrics 

* Total number of citations: 512 (WOS), 536 (Scopus), 732 (Google Scholar)
* h-index: 16 (WOS and Scopus), 18 (Google Scholar)

## Peer-Review Activity 

Referee for Physical Review Letters, Journal of Physics: Condensed Matter, Journal of Physics: Conference Series, MBPI - Crystals.


## Teaching activity

* 2020-present: Condensed Matter Physics (56 h, master's degree in Physics, University of Parma)
* 2019-2020 (2 years): course on "Fisica generale I" (mechanics and thermodynamics, 72 h, bachelor's degree in Engineering, University of Parma)
* Co-supervisor of a master student (Dept. of Physics and Astronomy, University of Bologna) and Ph.D. student (Dept. of Mathematical, Physical and Computer Sciences, University of Parma).

## Presentations to International Conferences, Workshops and Schools

10+ contributed talks at international conferences and 2 invited seminal activities.

## Honors and awards

2014: Awarded "ISMS Young Scientist Award" by the International Society for µSR Spectroscopy Executive Committee at the 13th International Conference on Muon Spin Rotation, Relaxation and Resonance.

## Projects and Proposals

* 8 experiments at large scale muon spin rotation facilities.
* currently PI of a High Performance Computer grant (ISCRA-B) awarded 1.5M CPUh at Cineca (Italy). Previously awarded multiple compute time allocations as PI in CINECA (Italy) and CSCS (Switzerland).
* currently part of the European Centre of Excellence MaX2, Materials design At the Exascale through affiliation with CNR Nano S3-Modena.


## Working Positions

Current position since March 2019: research associate (RTD-A) at the Department of Mathematical, Physical and Computer Sciences of the University of Parma, Italy.

### Past positions

* 13/02/2017 - 28/02/2019: Scientific Software Developer and HPC specialist at CINECA, Casalecchio di Reno, Italy.
  * Involved in European Projects for HPC, with a particular focus on the Materials at the Exascale (MaX)  CoE, which is devoted to the development of computational methods for materials science at the nanoscale. Main contribution: development of a GPU accelerated version of Quantum ESPRESSO, a DFT based massively parallel code.

* 16/01/2016 - 16/01/2017: Postdoctoral Fellow, Department of Physics and Earth Sciences, University of Parma, Parma (Italy) 
  * Post-doctoral research focused on the development and the validation of ab initio methods for the identification of the muon site in crystalline compounds. Work performed in the framework of the SINE2020 European research project.

* 24/12/2014 - 01/01/2017: Electronics and Software Developer, Independent contractor
  * Designed, assembled, developed and installed Kinopsys (www.kinopsys.com), an OpenCV based stereo camera software for 3D tracking of infrared markers currently in use at A.N.Svi. (Academy of Developmental Neuropsychology, Parma, Italy).
  
* 16/01/2015 - 16/07/2015: Postdoctoral Grant, Department of Physics and Earth Sciences, University of Parma, Parma (Italy) 
  *  Density Functional Theory (DFT) based simulations for the identification of muon embedding sites.
