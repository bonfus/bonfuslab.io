---
title: "Unfold.x for QE 6.1"
date: 2018-01-01T18:30:21-05:00
draft: false
type: "💻️"
---

Simple code to unfold band structures of first-principles 
supercell calculations.

https://bitbucket.org/bonfus/unfold-x
