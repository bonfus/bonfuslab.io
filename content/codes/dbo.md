---
title: "Double Adiabatic Approximation code for muSR spectroscopy"
date: 2014-12-29T18:30:21-05:00
draft: false
type: "💻️"
---

Collects and solved the Schrodinger equation using the double adiabatic
approximation for a muon in a crystal.

https://bitbucket.org/bonfus/dbo
