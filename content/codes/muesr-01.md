---
title: "MuESR and MuLFC releasd"
date: 2018-04-01T18:30:21-05:00
draft: false
type: "💻️"
---

First beta release of MuESR, a tool for calculating dipolar field sums for muon spin rotation and relaxation
spectroscopy.

https://github.com/bonfus/muesr \\
https://github.com/bonfus/muLFC
