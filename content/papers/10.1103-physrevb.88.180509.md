---
title: "s-wave pairing in the optimally doped LaO0.5F0.5BiS2superconductor"
date: 2013-11-01T00:00:00Z
draft: false
type: 🗞
---

G. Lamura, T. Shiroka, P. Bonfà, S. Sanna, R. De Renzi, C. Baines, H. Luetkens, J. Kajitani, Y. Mizuguchi, O. Miura, K. Deguchi, S. Demura, Y. Takano, M. Putti
Physical Review B

DOI: [10.1103/physrevb.88.180509](https://dx.doi.org/10.1103/physrevb.88.180509 "10.1103/physrevb.88.180509")

{{< bibtex >}}
@article{Lamura_2013,
doi = {10.1103/physrevb.88.180509},
url = {https://doi.org/10.1103%2Fphysrevb.88.180509},
year = 2013,
month = {nov},
publisher = {American Physical Society ({APS})},
volume = {88},
number = {18},
author = {G. Lamura and T. Shiroka and P. Bonf{\`{a}} and S. Sanna and R. De Renzi and C. Baines and H. Luetkens and J. Kajitani and Y. Mizuguchi and O. Miura and K. Deguchi and S. Demura and Y. Takano and M. Putti},
title = {s-wave pairing in the optimally doped {LaO}0.5F0.5BiS2superconductor},
journal = {Physical Review B}}
{{< /bibtex >}}
