---
title: "Magnetic states of MnP: Muon-spin rotation studies"
date: 2017-02-01T00:00:00Z
draft: false
type: 🗞
---

Khasanov, R., Amato, A., Bonfà, P., Guguchia, Z., Luetkens, H., Morenzoni, E., De Renzi, R., Zhigadlo, N.D.
Journal of Physics Condensed Matter

DOI: [10.1088/1361-648X/aa6391](https://dx.doi.org/10.1088/1361-648X/aa6391 "10.1088/1361-648X/aa6391")

{{< bibtex >}}
@article{Bonfà2017,
title = {Magnetic states of MnP: Muon-spin rotation studies},
journal = {Journal of Physics Condensed Matter},
year = {2017},
volume = {29},
number = {16},
author = {Khasanov,
 R. and Amato,
 A. and Bonf{\`a},
 P. and Guguchia,
 Z. and Luetkens,
 H. and Morenzoni,
 E. and De Renzi,
 R. and Zhigadlo,
 N.D.}}
{{< /bibtex >}}
