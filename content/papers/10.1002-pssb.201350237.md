---
title: "75As NQR signature of the isoelectronic nature of ruthenium for iron substitution in LaFe1−xRuxAsO"
date: 2014-02-01T00:00:00Z
draft: false
type: 🗞
---

Marcello Mazzani, Pietro Bonfà, Giuseppe Allodi, Samuele Sanna, Alberto Martinelli, Andrea Palenzona, Pietro Manfrinetti, Marina Putti, Roberto De Renzi
physica status solidi (b)

DOI: [10.1002/pssb.201350237](https://dx.doi.org/10.1002/pssb.201350237 "10.1002/pssb.201350237")

{{< bibtex >}}
@article{Mazzani_2014,
doi = {10.1002/pssb.201350237},
url = {https://doi.org/10.1002%2Fpssb.201350237},
year = 2014,
month = {feb},
publisher = {Wiley-Blackwell},
volume = {251},
number = {5},
pages = {974--979},
author = {Marcello Mazzani and Pietro Bonf{\`{a}} and Giuseppe Allodi and Samuele Sanna and Alberto Martinelli and Andrea Palenzona and Pietro Manfrinetti and Marina Putti and Roberto De Renzi},
title = {75As {NQR} signature of the isoelectronic nature of ruthenium for iron substitution in {LaFe}1-{xRuxAsO}},
journal = {physica status solidi (b)}}
{{< /bibtex >}}
