# coding: utf-8
from urllib.request import urlretrieve as urlretrieve
import xml.etree.ElementTree
import os.path
import json

table_header = """
| Cited-by   |      DOI      | Authors  | Title |
|------------|:-------------:|----------|-------|
"""

page_template = """---
title: "{title}"
date: {date}
draft: false
type: 🗞
---

{authors}
{journal}

DOI: [{doi}](https://dx.doi.org/{doi} "{doi}")

{{{{< bibtex >}}}}
{bibtex}
{{{{< /bibtex >}}}}
"""

def make_page(code, title, authors, journal, year, month, doi, bibtex):
    from dateutil import parser
    dt = parser.parse(year + "/" + month + "/1 00:00:00")
    date = dt.strftime("%Y-%m-%dT%H:%M:%SZ")
    with open('../content/papers/'+doi.replace('/','-').lower()+'.md', 'w') as f:
        f.write(page_template.format(title=title,
                                     journal=journal,
                                     date=date, 
                                     authors=', '.join(authors), 
                                     bibtex=bibtex.replace(',',',\n').replace('\n\n','\n'),
                                     doi=doi))

def make_table(code, title, authors, journal, year, month, doi, bibtex):
    urlretrieve('https://api.crossref.org/works/'+doi, code+'_crossref.json')
    with open(code+'_crossref.json','r') as f:
        data = json.load(f)
    with open('table.md','a') as f:
        citedby = data['message']['is-referenced-by-count']
        citedby = str(citedby)
        f.write ('| ' + ' | '.join([citedby, doi, ', '.join(authors), title]) + ' |\n')

def make_list(code, title, authors, journal, year, month, doi, bibtex):
    #urlretrieve('https://api.crossref.org/works/'+doi, code+'_crossref.json')
    with open(code+'_crossref.json','r') as f:
        data = json.load(f)
    with open('list.md','a') as f:
        msg = data['message']
        try:
            short_journal  = msg['short-container-title'][0]
        except IndexError:
            short_journal  = msg['container-title'][0]
        volume         = msg.get('volume', '??')
        article_number = msg.get('article-number', '??')
        authors        = ', '.join ("{} {}".format(x['given'], x['family']) for x in msg['author'])
        citedby        = str(msg['is-referenced-by-count'])

        citation       = '{} **{}**, {} ({})'.format(short_journal, volume, article_number, year)

        f.write ('_Title:_ {}  \n_Authors:_ {}  \n_Journal:_ {}  \n_DOI_: {}  \n_Personal contributions:_ \n\n\n'.format(title, authors, citation, doi))

if __name__ == '__main__':
    # Empty table
    open('table.md', 'w').write(table_header)
    #
    # Parse profile every now and then...
    # urlretrieve('https://pub.orcid.org/v2.0/0000-0001-6358-3037/record', 'record')
    #
    # Load profile already stored
    e = xml.etree.ElementTree.parse('record').getroot()
    
    l = e.findall('.//{http://www.orcid.org/ns/work}work-summary')
    for v in l:
        # Download only new articles.
        code = '0'
        for k, kv in v.items():
            if k == 'put-code':
                code = kv
            if k == 'path':
                if not os.path.isfile(code): 
                    urlretrieve('https://pub.orcid.org/v2.0'+kv, code)
        
        # parse authors and bibtex
        work = xml.etree.ElementTree.parse(code).getroot()
        authors = []
        for lll in work.findall('.//{http://www.orcid.org/ns/work}credit-name'):
            authors.append(lll.text)
        
        journal = work.find('.//{http://www.orcid.org/ns/work}journal-title').text
        #print(journal)
        
        pdate = v.find('.//{http://www.orcid.org/ns/common}publication-date')
        try:
            year = pdate[0].text
            month = pdate[1].text
        except:
            pass
        title = v.find('.//{http://www.orcid.org/ns/common}title').text

        try:
            bibtex = work.find('.//{http://www.orcid.org/ns/work}citation-value').text
        except:
            bibtex = ""
            print('Bibtex failed for: ', title)

        
        refs = v.findall('.//{http://www.orcid.org/ns/common}external-id-value')
        reftypes = v.findall('.//{http://www.orcid.org/ns/common}external-id-type')
        for i, t in enumerate(reftypes):
            if t.text == 'doi':
                doi = refs[i].text
        make_page(code, title, authors, journal, year, month, doi, bibtex)
        #make_list(code, title, authors, journal, year, month, doi, bibtex)
        #make_table(code, title, authors, journal, year, month, doi, bibtex)
